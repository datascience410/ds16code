
def test_predictor( target_table, pred ):
    results = {'pos-pos': 0,
                'neg-neg': 0,
                'false-pos': 0,
                'false-neg': 0
                }
    for row in target_table: #could change to big_table
        p = str( pred( row ) ) # survived (1) or not (0)
        a = str( row['Survived'] )
        if a == '1':
            if a == p:
                results['pos-pos'] += 1
            else:
                results['false-neg'] += 1
        else:
            #a == 0
            if a == p:
                results['neg-neg'] += 1
            else:
                results['false-pos'] += 1
    x = results
    y = (x['pos-pos'] + x['neg-neg'])/float( len(target_table) )
    print( 'accuracy: ' + str( y ))

    labels = ['neg-neg', 'pos-pos', 'false-neg', 'false-pos']
    
    sizes = [x['neg-neg'], x['pos-pos'], x['false-neg'], x['false-pos']]
    
    colors = ['yellowgreen', 'mediumpurple', 'lightblue', 'yellow']
    
    plt.pie(sizes,
            labels = labels,
            colors = colors,
            autopct = '%1.1f%%',
            shadow = True
            )
    
    plt.title('Results - total is ' + str( len(target_table) ),
                y=1.09, 
              fontsize=15)
    plt.axis('equal')
    plt.show()
    return "done"
