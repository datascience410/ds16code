
# coding: utf-8

# # Spark streaming
# This is a simple starting template for streaming. Before you can use Spark streaming (SS), you must configure Spark. 
# 
# 1. In your Spark installation, copy conf/spark_defaults.conf.template tp conf/spark_defaults.conf
# 
# 2. Edit $SPARK_HOME/conf/spark_defaults.conf, adding the path to the spark-streaming-mqtt-assembly_2.11-1.6.0.jar, below is the setting on my laptop:
# 
# <pre>spark.driver.extraClassPath /Users/norris/spark-1.6.0-bin-hadoop2.4/lib/spark-streaming-mqtt-assembly_2.11-1.6.0.jar
# </pre>
# 
# 3. After you do that, you have to restart Spark (or jupyter-notebook).

# In[1]:

get_ipython().magic('matplotlib inline')
import findspark
import os,sys,operator
findspark.init()  # You can also use the path to spark, i.e., findspark.init('/home/me/spark')

import pyspark
if not dir().count('sc'): sc = pyspark.SparkContext(appName="SoundMonitor") # Only do this once
print(sc.version)


# Load Spark Streaming packages and MQTT utilities.

# In[2]:

# For streaming
from pyspark.streaming import StreamingContext
from pyspark.streaming.mqtt import MQTTUtils

ssc = StreamingContext(sc, 1)
ssc.checkpoint("checkpoint")


# ## Implementation

# Set up the stream transformations. Note that this is not yet operating on any stream, all of these will be applied after issuing ssc.start().

# In[3]:

# broker URI
brokerUrl = "tcp://brix.d.cs.uoregon.edu:8100" # "tcp://iot.eclipse.org:1883"
# topic or topic pattern where temperature data is being sent
topic = "cis/soundtest"

mqttStream = MQTTUtils.createStream(ssc, brokerUrl, topic)


# First just print the data or do a very simple transformation and print it.

# In[4]:

if False:                  # Just change to True to activate
    mqttStream.pprint()    # Simply print messages as they arrive

    # Similar to word count, create pairs (soundVal, 1):
    soundLevels = mqttStream.map(lambda message: message.split(": ")[-1]).map(lambda val: (val,1))
    soundLevels.pprint()


# Next, let us create some more permanent statistics, in this case, count the occurences of each noise level. For additional information on aggregation operations on streams, see the [Spark documentation](https://docs.cloud.databricks.com/docs/latest/databricks_guide/08%20Spark%20Streaming/11%20Global%20Aggregations%20-%20updateStateByKey.html).

# In[5]:

def updateFunc(new_values, last_sum):
    return sum(new_values) + (last_sum or 0)

running_counts = mqttStream.flatMap( lambda line: [line.split(": ")[-1]] )            .map(lambda val: (int(val), 1))            .updateStateByKey(updateFunc)
        
#running_counts.pprint()


# In[6]:

import sys

def updateMin(new_values, last_min):
    return min( min(new_values), (last_min or sys.maxsize) ) 

running_min = mqttStream.flatMap( lambda line: [line.split(": ")[-1]] )            .map(lambda val: ('min',int(val)))            .updateStateByKey(updateMin)
running_min.pprint()


# In[7]:

ssc.start()
ssc.awaitTerminationOrTimeout(10)


# You can also take a streaming data structure (DStream or TransformedDStream), which are collections of RDDs.

# In[ ]:

all=[]
running_counts.foreachRDD( lambda x: all.append(x.collect()) )


# In[ ]:

from pyspark.sql import SQLContext, Row
from pyspark.sql.types import IntegerType
from pyspark.sql.functions import UserDefinedFunction

sqlContext = SQLContext(sc)  # Required to be able to work with data framessdf = sqlContext.createDataFrame(srdd)


# In[ ]:

# Convert to df
def look_for_values(rdd):
    if rdd.isEmpty(): return
    rdd.toDF().registerTempTable("NoiseLogs")
    #print(rdd.take(2))
    print("Noisy: ", 
          rdd.filter(lambda x: x[0] > 123)\
            .sortByKey(ascending=False)\
            .take(10))
running_counts.foreachRDD( look_for_values )


# In[ ]:

#schema = sqlContext.StructType([])
#df = sqlContext.createDataFrame(sc.emptyRDD(), schema)


# In[ ]:

def streamrdd_to_df(srdd):
    sdf = sqlContext.createDataFrame(srdd)
    sdf.show(n=2, truncate=False)
    return sdf

streaming_df = running_counts.flatMap(streamrdd_to_df)


# In[ ]:

ssc.start()
ssc.awaitTerminationOrTimeout(10)


# In[ ]:

ssc.stop()


# In[ ]:



