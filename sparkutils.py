from findspark import findspark
import os

import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size':20})

# Spark initialization
findspark.init() 
import pyspark
if not dir().count('sc'): sc = pyspark.SparkContext() # Only do this once

print("Using Spark version", sc.version)

from pyspark.sql import SQLContext, Row
from pyspark.sql.types import *
from pyspark.sql.functions import UserDefinedFunction

sqlCtx = SQLContext(sc)  # Required to be able to work with data frames

# https://github.com/seahboonsiew/pyspark-csv
import pyspark_csv as pycsv

def loadDF(filename):
    """
    Load and parse filename as pyspark.sql.DataFrame
    using pyspark_csv.py
    """
    plain = sc.textFile(filename)
    df = pycsv.csvToDataFrame(sqlCtx, plain, sep=',')
    return df

def get_stats_dict(df,colname):
    # Example use: agestats = get_stats_dict(new_train, 'Age')
    return dict(map(lambda x: [x['summary'],x[colname]], df.describe().collect()))


