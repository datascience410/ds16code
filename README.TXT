To work with the IPython notebooks on your own machine, follow these steps.

1. Download and install Anaconda from https://www.continuum.io/downloads
    Pay attention to where the installation goes, for example, on a Mac, note the location 
    where it's installed if you specified it, otherwise it probably ended up in /anaconda.
    
2. Open a Terminal and set the path to Anaconda; to make it permanent, add this line to 
   ~/.bashrc. For example, if you installed Anaconda in /anaconda 
   
   export PATH=/anaconda/bin:$PATH
   
3. Go to the directory where you wish to create and edit notebooks, e.g., your 
   clone of https://bitbucket.org/datascience410/ds16code.git and run
   
   ipython notebook

Working with pyspark notebooks
==============================

These are directions for using Jupyter, which replaces IPython. Please read 
the migration document (http://jupyter.readthedocs.org/en/latest/migrating.html) if 
you have existing IPython configurations. If you want to continue using IPython 
(instead of switching to Jupyter), follow these directions: 
http://spark.apache.org/docs/latest/programming-guide.html#initializing-spark

If you are on a Mac or Windows, you can use the Anaconda Launcher (graphical). 

For command-line only, follow these instructions. In the ds16code directory:

1. Make sure Anaconda's bin/ directory is in your path (verify with "which python")

2. Obtain and install the findspark package:
	git clone https://github.com/minrk/findspark.git
	cd findspark
	pip install findspark

3. Set the SPARK_HOME environment variable to the top-level Spark directory.

4. Start the notebook server, by default it will use port 8888. Open or create a new notebook.
	jupyter-notebook

5. In your notebook, execute the code below before using any Spark functions. That's it!

import findspark
import os
findspark.init()
import pyspark
sc = pyspark.SparkContext()

This should also work if you use Python3, which is not installed by default in Anaconda. 
If you wish to switch to Python3, follow these instructions: 
https://www.continuum.io/content/python-3-support-anaconda (then repeat the above 
steps with python3). 
