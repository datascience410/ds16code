"""
Streaming k-means (Boyana's version)
    `$ bin/spark-submit k-means.py`
"""
from __future__ import print_function

import numpy as np

from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.mqtt import MQTTUtils

def find_center(point, centers):
    shortest = float('inf')
    d = {}
    for center_id, value in centers.items():
        d[center_id] = np.sqrt(((np.array(value[0])-np.array(point))**2).sum(axis=0))
    return (point, min(d, key=d.get))

def update_centers(batch, last_centers):
    # batch is a list of stream elements, e.g.,  [(126.0, 0.000346)]
    # last_centers is the current centers dictionary, e.g.:
    # {1: [[123.0, 0.000277], 1], 2: [[130, 0.0001], 0], 3: [[150, 0.0001], 0], 4: [[200, 0.0001], 0]})
    if not last_centers:
        # Initial centers
        centers = {1:[[123,0.0001],0], 2:[[130,0.0001],0], 3:[[150,0.0001],0], 4:[[200,0.0001],0]}
    else:
        centers = last_centers
    #find the nearest centers for each batch point
    d = dict(map( lambda x: find_center(x, centers), batch ))
    if d: print("Batch centers: ", d)
        
    #update the centers
    for x in batch:
        c_id = d[x]
        if not centers.get(c_id): centers[c_id] = [[],0]
        centers[c_id][1] += 1
        eta = 1.0 / centers[c_id][1]
        wk = np.array(centers[c_id][0])
        xvec = np.array(x)
        # Nudge
        centers[c_id][0] = (wk + eta*(xvec - wk)).tolist()
        
    return centers
          
if __name__ == "__main__":
    sc = SparkContext(appName="PythonStreamingKMeans")
    ssc = StreamingContext(sc, 1)
    ssc.checkpoint("checkpoint")
    ssc.remember(30)

    # broker URI
    brokerUrl = "tcp://brix.d.cs.uoregon.edu:8100" # "tcp://iot.eclipse.org:1883"
    topic = "cis/soundtest/preprocessed"

    soundStream = MQTTUtils.createStream(ssc, brokerUrl, topic)
    kmeans = soundStream.map(lambda line: tuple( float(x) for x in line.split(",") ) )\
        .map( lambda pt: ('centers',pt) )\
        .updateStateByKey(update_centers)

    kmeans.pprint()

    
    ssc.start()
    ssc.awaitTermination()
